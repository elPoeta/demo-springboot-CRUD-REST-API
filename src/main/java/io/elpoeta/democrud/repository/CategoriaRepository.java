
package io.elpoeta.democrud.repository;

import io.elpoeta.democrud.domain.Categoria;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author elPoeta
 */
public interface CategoriaRepository extends CrudRepository<Categoria, Long>{
    
}
